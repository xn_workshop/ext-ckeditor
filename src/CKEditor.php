<?php

namespace Xn\CKEditor;

use Xn\Admin\Form\Field;

class CKEditor extends Field
{
	protected static $js = [
        '/vendor/xn-ckeditor/ckeditor/ckeditor.js',
    ];

    protected $view = 'xn-ckeditor::ckeditor';

    public function render()
    {
        $config = (array)CKEditorConf::config('config');
        $config = json_encode(array_merge($config, $this->options));

        $this->script = <<<EOT
        Array.prototype.forEach.call(document.getElementsByClassName('xn-ckeditor'), function(item) {
            if (item.classList.contains("xn-ckeditor") && !item.classList.contains("active")) {
                item.classList.add("active");
                CKEDITOR.replace(item, {$config});
            }

        });
        EOT;

        return parent::render();
    }
}