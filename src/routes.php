<?php

Route::any('/ckfinder/connector', '\Xn\CKEditor\Controller\CKFinderController@requestAction')
    ->name('ckfinder_connector');

Route::any('/ckfinder/browser', '\Xn\CKEditor\Controller\CKFinderController@browserAction')
    ->name('ckfinder_browser');
